const amqp = require('amqplib/callback_api');

const getDate = function() {
    var days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

    var now = new Date();

    str = days[now.getDay()] + ", " + now.getDate() + " " + months[now.getMonth()] + " " + now.getFullYear() + " " + now.getHours() +":" + now.getMinutes() + ":" + now.getSeconds();
    return str;
}


amqp.connect('amqp://0806444524:0806444524@152.118.148.95:5672/%2f0806444524', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = '1606879230_TOPIC';
        var routingKey = "SagabRoutingKey";
        channel.assertExchange(exchange, 'direct', {
            durable: false
        });
        setInterval(() => {
            const msgObject = {type: "clock", data: getDate()}
            channel.publish(exchange, routingKey, Buffer.from(JSON.stringify(msgObject)));
            console.log(" [x] Sent %s: '%s'", routingKey, JSON.stringify(msgObject));
        }, 1000);
    });
});
